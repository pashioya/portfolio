import * as THREE from "../node_modules/three/build/three.module.js";
import { OrbitControls } from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import "/style.css";

const scene = new THREE.Scene();
const geometry = new THREE.SphereGeometry(1, 64, 64);

const material = new THREE.MeshStandardMaterial({
    color: 0xff0000,
});
const sphere = new THREE.Mesh(geometry, material);
scene.add(sphere);

const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    101
);
camera.position.z = 5;

const canvas = document.querySelector(".webgl");
const renderer = new THREE.WebGLRenderer({ canvas });
const light = new THREE.PointLight(0xffffff, 1, 100);
light.position.set(0, 11, 10);
scene.add(light);
renderer.setSize(window.innerWidth, window.innerHeight);

renderer.render(scene, camera);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;
controls.autoRotate = true;
controls.enablePan = false;
controls.enableZoom = false;

// set the light position to follow the mouse
// document.addEventListener("mousemove", animateLight);

// function animateLight(event) {
//     // convert the mouse position to a value between 0 and 100
//     light.position.x = (event.clientX / window.innerWidth) * 100;
//     light.position.y = (event.clientY / window.innerHeight) * 100;
//     light.position.z = 0;

//     scene.add(light);
// }

window.addEventListener("resize", () => {
    // update the camera
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    // update the renderer
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

const loop = () => {
    controls.update();
    window.requestAnimationFrame(loop);
    renderer.render(scene, camera);
};

loop();
